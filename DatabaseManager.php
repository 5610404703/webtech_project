<?php
class Database {
  private $hostname = 'localhost';
  private $username = 'root';
  private $password = '';
  private $dbname = 'personal';
  private $connection;
  public function __construct (){
    try {
      $this->connection = new PDO("mysql:host={$this->hostname};" . "dbname={$this->dbname}",
      $this->username,
      $this->password);
      $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    } catch (Exception $e) {
      die('Database Connection Error: '. $e->getMessage());
    }
  }

  public function query($sql) {
    try {
      $stmt = $this->connection->query($sql);
    } catch (Exception $e) {
      die($e->getMessage());
    }
    return $stmt->fetchAll(PDO::FETCH_OBJ);
  }

  public function insert ($sql, $array) {
    // $sql = 'INSERT INTO employees(LastName, FirstName)
    // VALUES(:lastname, :firstname)';
    // $array = array(
    //   ':lastname'=> $lastname,
    //   ':firstname' => $firstname,
    //   );
    try {
      $stmt = $this->connection->prepare($sql);
      $stmt->execute();
      if ($stmt->rowCount() >= 1) return true;
      else return false;
    } catch (Exception $e) {
      die($e->getMessage());
    }
  }

  public function edit($sql, $array){
      try{
        $stmt = $this->connection->prepare($sql);
        $stmt->execute($array);
        if ($stmt->rowCount() >= 1) return true;
        else return false;
      } catch (Exception $e) {
        die($e->getMessage());
      }
  }
}
?>
