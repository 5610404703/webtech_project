<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<style>

<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
body {
    font-family: "Lato", sans-serif;
    transition: background-color .5s;
}
h1 { color: #7c795d; font-family: 'Trocchi', serif; font-size: 45px; font-weight: normal; line-height: 48px; margin: 0; }
.MainPage .ProfilePage{
  display: none;
}
input.hidden {
    position: absolute;
    left: -9999px;
}

#profile-image {
    cursor: pointer;
}

.sidenav {
    height: 100%;
    width: 0;
    position: fixed;
    z-index: 1;
    top: 0;
    left: 0;
    background-color: #111;
    overflow-x: hidden;
    transition: 0.5s;
    padding-top: 60px;
}

.sidenav a {
    padding: 8px 8px 8px 32px;
    text-decoration: none;
    font-size: 25px;
    color: #818181;
    display: block;
    transition: 0.3s
}

.sidenav a:hover, .offcanvas a:focus{
    color: #f1f1f1;
}

.sidenav .closebtn {
    position: absolute;
    top: 0;
    right: 25px;
    font-size: 36px;
    margin-left: 50px;
}
.tabcontent {
    float: right;
    padding: 0px 12px;
    
    width: 70%;
    border-left: none;
}

#main {
    transition: margin-left .5s;
    padding: 16px;
}

@media screen and (max-height: 450px) {
  .sidenav {padding-top: 15px;}
  .sidenav a {font-size: 18px;}
}
</style>
</head>
<body>

<div id="mySidenav" class="sidenav">
  <h1><span style="font-size:30px;cursor:pointer">Account</span></h1>
  <form id="submit" name="submit" action="image.php" method='post' enctype="multipart/form-data">
    <input id="profile-image-upload" name='image'  class="hidden" type="file">
  </form>
  <div id="profile-image">
    <img src="<?php 
    require_once __DIR__."/databaseconnect.php";
    $db=new Database();
      $db->connect();
      $result=mysqli_query($db->getConn(),"SELECT member.idpicture,image.idimage,image.img FROM member,image 
                        WHERE member.idpicture = image.idimage");
      while($row=mysqli_fetch_array($result)){
        echo 'data:image;base64,'.$row[2].'';
      }
      $db->disconnect();
    
    ?>" alt="Fjords" style="width:100%">
  </div>
  <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a> 
  <a href="#" onclick="openType(event, 'Admin')" >Admin</a>
  <a href="#" onclick="openType(event, 'Teacher')">Teacher</a>
  <a href="#" onclick="openType(event, 'Student')">Student</a>
  <a href="#" onclick="openType(event, 'Upload')">Upload</a>
  <a href="#">Logout</a>
</div>

<div id="Admin" class="tabcontent">
  <form name='Person' value='Admin' method='post' action="<?php echo $_SERVER['PHP_SELF']; ?>">
    <h1>Admin</h1>
    Account:<br>
    <input type="text" class="form-control" name="idadmin">
    <br>
    Password:<br>
    <input type="password" class="form-control" name="password" id="txtNewPassword">
    <br>
    Re-Password:<br>
    <input type="password" class="form-control" name="repassword" id="txtConfirmPassword">
    <p class="registrationFormAlert" id="divCheckPasswordMatch"></p>
    Firstname:<br>
    <input type="text" class="form-control" name="firstname">
    <br>
    Lastname:<br>
    <input type="text" class="form-control" name="lastname"><br>
    E-mail:<br>
    <input type="text" class="form-control" name="email"><br>
    <button type="submit"  name='submitAdmin' class="btn btn-default">Submit</button>
  </form>
</div>

<div id="Teacher" class="tabcontent">
  <form name='Person' value='Teacher' method='post' action="<?php echo $_SERVER['PHP_SELF']; ?>">
    <h1>Teacher</h1>
    Account:<br>
    <input type="text" class="form-control" name="idadmin">
    <br>
    Password:<br>
    <input type="password" class="form-control" name="password" id="txtNewPassword">
    <br>
    Re-Password:<br>
    <input type="password" class="form-control" name="repassword" id="txtConfirmPassword">
    <p class="registrationFormAlert" id="divCheckPasswordMatch"></p>
    Firstname:<br>
    <input type="text" class="form-control" name="firstname">
    <br>
    Lastname:<br>
    <input type="text" class="form-control" name="lastname"><br>
    E-mail:<br>
    <input type="text" class="form-control" name="email"><br>
    <select name='role'>
            <option value="volvo">Lecture</option>
            <option value="saab">Lab</option>
    </select>
    <button type="submit" name='submitTeacher' class="btn btn-default">Submit</button>
  </form>
</div>

<div id="Student" class="tabcontent">
  <form name='Person' value='Student' method='post' action="<?php echo $_SERVER['PHP_SELF']; ?>">
    <h1>Student</h1>
    Account:<br>
    <input type="text" class="form-control" name="idadmin">
    <br>
    Password:<br>
    <input type="password" class="form-control" name="password" id="txtNewPassword">
    <br>
    Re-Password:<br>
    <input type="password" class="form-control" name="repassword" id="txtConfirmPassword">
    <p class="registrationFormAlert" id="divCheckPasswordMatch"></p>
    Firstname:<br>
    <input type="text" class="form-control" name="firstname">
    <br>
    Lastname:<br>
    <input type="text" class="form-control" name="lastname"><br>
    E-mail:<br>
    <input type="text" class="form-control" name="email"><br>
    <button type="submit" onclick="closeNav()" name='submitStudent' class="btn btn-default">Submit</button>
  </form>
</div>
<div id="Upload" class="tabcontent">
  <form name='Person' value='Upload' method='POST' action="upload.php" enctype="multipart/form-data">
    <input type='file' id='file' name='file' >
    <button type="submit" name='submit' class="btn btn-default">Submit</button>
  </form>
</div>

<!-- <div id="main">
  <span style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776; </span>
</div>-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script>
$(document).ready(function () {
  openNav();
   $("#txtConfirmPassword").keyup(checkPasswordMatch);
});
$('#profile-image').click(function() {
    $('#profile-image-upload').click();
    $('#profile-image-upload').change(function() {
      $('#submit').submit();
    });
});
function openNav() {
    document.getElementById("mySidenav").style.width = "30%";
    //document.getElementById("main").style.marginLeft = "250px";
    document.body.style.backgroundColor = "rgba(0,0,0,0.4)";
}

function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
    //document.getElementById("main").style.marginLeft= "0";
    document.body.style.backgroundColor = "white";
}
function openType(evt, type) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    document.getElementById(type).style.display = "block";
    //closeNav();
}
function checkPasswordMatch() {
    var password = $("#txtNewPassword").val();
    var confirmPassword = $("#txtConfirmPassword").val();

    if (password != confirmPassword){
        document.getElementById("divCheckPasswordMatch").style.color = "red";
        $("#divCheckPasswordMatch").html("Passwords do not match!");
      }
    else
        $("#divCheckPasswordMatch").html("Passwords match.");
}
tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }

</script>
     
</body>
<?php
    require_once __DIR__."/databaseconnect.php";
    $db=new Database();
    $db->connect();
      
    if(isset($_POST['submitAdmin'])){
    $account=$_POST['idadmin'];
    $password=$_POST['password'];
    $firstname=$_POST['firstname'];
    $lastname=$_POST['lastname'];
    $email=$_POST['email'];
    $sql ="INSERT INTO admin (idadmin, password, firstname,lastname,email,idpicture,type) VALUES ('$account','$password','$firstname','$lastname','$email',1,'Admin')";
    if ($db->getConn()->query($sql) === TRUE) {
    echo "New record created successfully";
    } else {
    echo "Error: " . $sql . "<br>" . $db->getConn()->error;
        }   
  }
  else if(isset($_POST['submitTeacher'])){
    $account=$_POST['idadmin'];
    $password=$_POST['password'];
    $firstname=$_POST['firstname'];
    $lastname=$_POST['lastname'];
    $email=$_POST['email'];
    $role=$_POST['role'];
    $sql ="INSERT INTO teacher (idteacher, password, firstname,lastname,email,idpicture,type) VALUES ('$account','$password','$firstname','$lastname','$email',1,'$role')";
        if ($db->getConn()->query($sql) === TRUE) {
    echo "New record created successfully";
    } else {
    echo "Error: " . $sql . "<br>" . $db->getConn()->error;
        }   
  }
  else if(isset($_POST['submitStudent'])){
    $account=$_POST['idadmin'];
    $password=$_POST['password'];
    $firstname=$_POST['firstname'];
    $lastname=$_POST['lastname'];
    $email=$_POST['email'];
    $sql ="INSERT INTO student (idstudent, password, firstname,lastname,email,idpicture,type) VALUES ('$account','$password','$firstname','$lastname','$email',1,'Student')";
        if ($db->getConn()->query($sql) === TRUE) {
    echo "New record created successfully";
    } else {
    echo "Error: " . $sql . "<br>" . $db->getConn()->error;
        }   
  }
?>

</html> 
