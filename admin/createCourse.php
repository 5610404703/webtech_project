<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>Create Course</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <link rel="apple-touch-icon" href="../apple-touch-icon.png">
        <link rel="stylesheet" href="../css/bootstrap.min.css">
        <link rel="stylesheet" href="../css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="../css/createCstyle.css">
        <script src="../js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>

    </head>
    <body>
        <nav class="navbar navbar-fixed-top" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="../admin/profileAdmin.php"><span><img alt="Brand" src="<?php 
                    require_once __DIR__."/databaseconnect.php";
                    $db=new Database();
                    $db->connect();
                    $idadmin=$_COOKIE['username'];
                    $result=mysqli_query($db->getConn(),"SELECT admin.idpicture,image.idimage,image.img FROM admin,image 
                        WHERE admin.idpicture = image.idimage && '$idadmin'=admin.idadmin");
                    while($row=mysqli_fetch_array($result)){

                        echo 'data:image;base64,'.$row[2].'';
                    }
                    $db->disconnect();
    
                    ?>" width="30" height="30"></span>&nbsp;&nbsp;<?php  
                                                            $idadmin=$_COOKIE['username'];
                                                            echo $idadmin;
                                                                ?></a>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="../admin/profileAdmin.php">Profile</a></li>
                        <li><a href="../admin/createAccount.php">Create Account</a></li>
                        <li><a href="../admin/createCourse.php">Create Course</a></li>
                        <li><a href="../admin/printPassStu.php">Print</a></li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li><a  href="../login.php">Logout</a></li>
                    </ul>
                </div>
            </div>
        </nav>


        <div class="createCourseForm">
            <h1>Create Course</h1>
            <form name='Person' value='Upload' method='POST' action="<?php echo $_SERVER['PHP_SELF']; ?>" enctype="multipart/form-data">
                <input type='file' id='file' name='file' >
                <button id="btnSubmit" type="submit" name="submit" class="btn btn-primary">Submit</button>
                <button id="btnCancel" type="reset" class="btn btn-danger">Cancel</button>
            </form>

        </div>

        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>

        <script src="js/vendor/bootstrap.min.js"></script>

        <script>
            (function (b, o, i, l, e, r) {
                b.GoogleAnalyticsObject = l;
                b[l] || (b[l] =
                        function () {
                            (b[l].q = b[l].q || []).push(arguments)
                        });
                b[l].l = +new Date;
                e = o.createElement(i);
                r = o.getElementsByTagName(i)[0];
                e.src = '//www.google-analytics.com/analytics.js';
                r.parentNode.insertBefore(e, r)
            }(window, document, 'script', 'ga'));
            ga('create', 'UA-XXXXX-X', 'auto');
            ga('send', 'pageview');
        </script>
    </body>
<?php
    require_once __DIR__."/databaseconnect.php";
    $db=new Database();
    $db->connect();
    if(isset($_POST['submit'])){
    //validate whether uploaded file is a csv file
    $csvMimes = array('text/x-comma-separated-values', 'text/comma-separated-values', 'application/octet-stream', 'application/vnd.ms-excel', 'application/x-csv', 'text/x-csv', 'text/csv', 'application/csv', 'application/excel', 'application/vnd.msexcel', 'text/plain');
    $name=($_FILES['file']['name']);
    if(!empty($_FILES['file']['name']) && in_array($_FILES['file']['type'],$csvMimes)){
        if(is_uploaded_file($_FILES['file']['tmp_name'])){
            //open uploaded csv file with read only mode
            $csvFile = fopen($_FILES['file']['tmp_name'], 'r');
                   
            //skip first line
            fgetcsv($csvFile);
            
            //parse data from csv file line by line
            while(($line = fgetcsv($csvFile)) !== FALSE){
                //check whether member already exists in database with same email
                $sql ="INSERT INTO student_subject (idstudent, idsubject, namestudent,namesubject,sectorid,sectorname,credit,term) VALUES ('$line[0]','$line[1]','$line[2]','$line[3]','$line[4]','$line[5]','$line[6]','$line[7]')";
                if ($db->getConn()->query($sql) === TRUE) {
                    //echo "New record created successfully";
                } else {
                    echo "Error: " . $sql . "<br>" . $db->getConn()->error;
                    }
            }
            
            //close opened csv file
            fclose($csvFile);

            $qstring = '?status=succ';
        }else{
            $qstring = '?status=err';
        }
    }else{
        $qstring = '?status=invalid_file';

    }
}

//redirect to the listing page
//header("Location: index.php".$qstring);

?>
</html>
