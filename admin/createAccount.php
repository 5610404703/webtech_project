<html>
    <head>
        <title>Create Account</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <link rel="apple-touch-icon" href="../apple-touch-icon.png">
        <link rel="stylesheet" href="../css/bootstrap.min.css">
        <link rel="stylesheet" href="../css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="../css/createAccStyle.css">
        <script src="../js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    </head>
    <body>
        <nav class="navbar navbar-fixed-top" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="../admin/profileAdmin.php"><span><img alt="Brand" src="<?php 
                    require_once __DIR__."/databaseconnect.php";
                    $db=new Database();
                    $db->connect();
                    $idadmin=$_COOKIE['username'];
                    $result=mysqli_query($db->getConn(),"SELECT admin.idpicture,image.idimage,image.img FROM admin,image 
                        WHERE admin.idpicture = image.idimage && '$idadmin'=admin.idadmin");
                    while($row=mysqli_fetch_array($result)){

                        echo 'data:image;base64,'.$row[2].'';
                    }
                    $db->disconnect();
    
                    ?>" width="30" height="30"></span>&nbsp;&nbsp;<?php  
                                                            $idadmin=$_COOKIE['username'];
                                                            echo $idadmin;
                                                                ?></a>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        <li class="active">
                            <a href="../admin/profileAdmin.php">Profile</a>
                        </li>
                        <li>
                            <a href="../admin/createAccount.php">Create Account</a>
                        </li>
                        <li>
                            <a href="../admin/createCourse.php">Create Course</a>
                        </li>
                        <li>
                            <a href="../admin/printPassStu.php">Print</a>
                        </li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <a href="../login.php">Logout</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <div class="col-sm-offset-1 col-md-10">
            <h1>Create Account</h1>
            <hr>
            <form class="form-horizontal" role="form" method="POST" action="<?php echo $_SERVER['PHP_SELF']; ?>">
                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-2">
                        <label for="inputtype" class="control-label">Type</label>
                    </div>
                    <div class="col-sm-4 selectContainer">
                        <select class="form-control" name="inputtype" id="inputtype">
                            <option value="">Choose a type</option>
                            <option value="student">Student</option>
                            <option value="teacher">Teacher</option>
                            <option value="admin">Admin</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-2">
                        <label for="inputFName" class="control-label">First name</label>
                    </div>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="inputFName" name="inputFName" placeholder="First name">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-2">
                        <label for="inputLName" class="control-label">Last name</label>
                    </div>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="inputLName" name="inputLName" placeholder="Last name">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-2">
                        <label for="username" class="control-label">Username</label>
                    </div>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="username" name="username" placeholder="Username">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-2">
                        <label for="inputPassword" class="control-label">Password</label>
                    </div>
                    <div class="col-sm-4">
                        <input type="password" class="form-control" id="inputPassword" name="inputPassword" placeholder="Password">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-2">
                        <label for="email" class="control-label">E-mail</label>
                    </div>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="email" name="email" placeholder="E-mail">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-9 col-sm-3">
                        <button type="submit" name="submit" class="btn btn-success">Submit</button>
                        <button type="reset" class="btn btn-danger">Cancle</button>
                    </div>
                </div>
                <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
                <script>
                    window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')
                </script>
                <script src="../js/vendor/bootstrap.min.js"></script>
                <script>
                    (function (b, o, i, l, e, r) {
                        b.GoogleAnalyticsObject = l;
                        b[l] || (b[l] =
                                function () {
                                    (b[l].q = b[l].q || []).push(arguments)
                                });
                        b[l].l = +new Date;
                        e = o.createElement(i);
                        r = o.getElementsByTagName(i)[0];
                        e.src = '//www.google-analytics.com/analytics.js';
                        r.parentNode.insertBefore(e, r)
                    }(window, document, 'script', 'ga'));
                    ga('create', 'UA-XXXXX-X', 'auto');
                    ga('send', 'pageview');
                </script>
            </form>
        </div>


    </body>
    <?php
        require_once __DIR__."/databaseconnect.php";
        $db=new Database();
        $db->connect();
        if(isset($_POST['submit'])){
            $type=$_POST['inputtype'];
            $account=$_POST['username'];
            $password=$_POST['inputPassword'];
            $firstname=$_POST['inputFName'];
            $lastname=$_POST['inputLName'];
            $email=$_POST['email'];
            if ($type=='admin'){
                $sql ="INSERT INTO $type (idadmin, password, firstname,lastname,email,idpicture,type) VALUES ('$account','$password','$firstname','$lastname','$email',1,'$type')";
            }
            if ($type=='teacher'){
                $sql ="INSERT INTO $type (idteacher, password, firstname,lastname,email,idpicture,type) VALUES ('$account','$password','$firstname','$lastname','$email',1,'$type')";
            }
            if ($type=='student'){
                $sql ="INSERT INTO $type (idstudent, password, firstname,lastname,email,idpicture,type) VALUES ('$account','$password','$firstname','$lastname','$email',1,'$type')";
            }
            
            if ($db->getConn()->query($sql) === TRUE) {
                //echo "New record created successfully";
            } else {
                echo "Error: " . $sql . "<br>" . $db->getConn()->error;
            }
        }  
    ?>
</html>