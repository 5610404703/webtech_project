<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>Profile Admin</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <link rel="apple-touch-icon" href="../apple-touch-icon.png">
        <link rel="stylesheet" href="../css/bootstrap.min.css">
        <link rel="stylesheet" href="../css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="../css/profile.css">
        <script src="../js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>

    </head>
    <body>
        <nav class="navbar navbar-fixed-top" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="../admin/profileAdmin.php"><span><img alt="Brand" src="<?php 
                    require_once __DIR__."/databaseconnect.php";
                    $db=new Database();
                    $db->connect();
                    $idadmin=$_COOKIE['username'];
                    $result=mysqli_query($db->getConn(),"SELECT admin.idpicture,image.idimage,image.img FROM admin,image 
                        WHERE admin.idpicture = image.idimage && '$idadmin'=admin.idadmin");
                    while($row=mysqli_fetch_array($result)){

                        echo 'data:image;base64,'.$row[2].'';
                    }
                    $db->disconnect();
    
                    ?>" width="30" height="30"></span>&nbsp;&nbsp;<?php  
                                                            $idadmin=$_COOKIE['username'];
                                                            echo $idadmin;
                                                                ?></a>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="../admin/profileAdmin.php">Profile</a></li>
                        <li><a href="../admin/createAccount.php">Create Account</a></li>
                        <li><a href="../admin/createCourse.php">Create Course</a></li>
                        <li><a href="../admin/printPassStu.php">Print</a></li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li><a  href="../login.php">Logout</a></li>
                    </ul>
                </div>
            </div>
        </nav>

        <div class="col-md-3">
            <div class="well">
                <a href="#"><img src="<?php 
                    require_once __DIR__."/databaseconnect.php";
                    $db=new Database();
                    $db->connect();
                    $idadmin=$_COOKIE['username'];
                    $result=mysqli_query($db->getConn(),"SELECT admin.idpicture,image.idimage,image.img FROM admin,image 
                        WHERE admin.idpicture = image.idimage && '$idadmin'=admin.idadmin");
                    while($row=mysqli_fetch_array($result)){

                        echo 'data:image;base64,'.$row[2].'';
                    }
                    $db->disconnect();
    
                    ?>" class="img-thumbnail" alt="Display"> </a>
                <form id="submit" name="submit" action="image.php" method='post' enctype="multipart/form-data">
                <label class="btn btn-primary" for="my-file-selector">
                    <input id="my-file-selector" name='image' type="file" style="display:none; margin-top: 20px;">
                    Choose file
                </label>
                </form>
               
            </div>
        </div>

        <div class="col-md-9">
            <div class="profile">
                <h2> Staff </h2>
                <hr>
                <table id="profileTable" class="table table-striped">
                    <tbody>             
                        <tr>
                            <td>Firstname</td>
                            <?php
                            $firstname=$_COOKIE['firstname'];
                            echo "<td id='firstname'>$firstname</td>";
                            ?>
                        </tr>
                        <tr>
                            <td>Lastname</td>
                            <?php
                            $lastname=$_COOKIE['lastname'];
                            echo "<td id='lastname'>$lastname</td>";
                            ?>
                        </tr>
                        <tr>
                            <td>ID</td>
                            <?php
                            $idadmin=$_COOKIE['username'];
                            echo "<td id='id'>$idadmin</td>";
                            ?>
                        </tr>
                        <tr>
                            <td>Password</td>
                            <?php
                            $password=$_COOKIE['password'];
                            echo "<td id='password' type='password'>$password</td>";
                            ?>
                        </tr>
                    </tbody>
                </table>
               
                <button id="btnModify" type="button" class="btn btn-primary">Modify</button>
            </div>
            <div class="profileMo">
                <h2> Staff </h2>
                <table id="ProfileModifyTable" class="table table-striped">
                    <tbody>
                        <tr>
                            <td>Status</td>
                            <td>John</td>
                        </tr>
                        <tr>
                            <td>Firstname</td>
                            <td><input type="text" class="firstnameMo" id="firstnameMo"></td>
                        </tr>
                        <tr>
                            <td>Lastname</td>
                            <td><input type="text" class="lastnameMo" id="lastnameMo"></td>
                        </tr>
                        <tr>
                            <td>Username</td>
                            <td><input type="text" class="idMo" id="idMo"></td>
                        </tr>
                        <tr>
                            <td>Password</td>
                            <td><input type="password" class="passwordMo" id="passwordMo"></td>
                        </tr>
                    </tbody>         
                </table>
                <button id="btnMoSubmit" type="submit" class="btn btn-primary">Summit</button>
                <button id="btnMoCancel" type="reset" class="btn btn-danger">Cancel</button>
            </div>
        </div>

        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>

        <script src="js/vendor/bootstrap.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <script>
            $('#my-file-selector').click(function() {
                $('#my-file-selector').change(function() {
                $('#submit').submit();
            });
        });
        </script>
        <script>
            (function (b, o, i, l, e, r) {
                b.GoogleAnalyticsObject = l;
                b[l] || (b[l] =
                        function () {
                            (b[l].q = b[l].q || []).push(arguments)
                        });
                b[l].l = +new Date;
                e = o.createElement(i);
                r = o.getElementsByTagName(i)[0];
                e.src = '//www.google-analytics.com/analytics.js';
                r.parentNode.insertBefore(e, r)
            }(window, document, 'script', 'ga'));
            ga('create', 'UA-XXXXX-X', 'auto');
            ga('send', 'pageview');
        </script>
    </body>
</html>
