<html>
    <head>
        <title>Print Account Data</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <link rel="apple-touch-icon" href="../apple-touch-icon.png">
        <link rel="stylesheet" href="../css/bootstrap.min.css">
        <link rel="stylesheet" href="../css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="../css/createAccStyle.css">
        <script src="../js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
        <style>
            .tabs {
                padding-top: 130px;
                padding-left: 120px;
                padding-right: 120px;
                /*text-align: center;*/
            }

            .showtable{
                width: 70%;
                padding-top: 40px;
                padding-left: 240px;
            }
            .table-bordered {
                /*margin-top: 40px;*/
                background-color: #f9f9f9;
            }

            th {
                background-color: #b9def0;
                text-align: center;
            }

            td:nth-child(1) {
                text-align: center;
            }

            td:nth-child(3) {
                text-align: center;
            }
            .form-group {
                padding-top: 20px;
                padding-left: 800px;
            }

        </style>
    </head>
    <body>
        <nav class="navbar navbar-fixed-top" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="../admin/profileAdmin.php"><span><img alt="Brand" src="<?php 
                    require_once __DIR__."/databaseconnect.php";
                    $db=new Database();
                    $db->connect();
                    $idadmin=$_COOKIE['username'];
                    $result=mysqli_query($db->getConn(),"SELECT admin.idpicture,image.idimage,image.img FROM admin,image 
                        WHERE admin.idpicture = image.idimage && '$idadmin'=admin.idadmin");
                    while($row=mysqli_fetch_array($result)){

                        echo 'data:image;base64,'.$row[2].'';
                    }
                    $db->disconnect();
    
                    ?>" width="30" height="30"></span>&nbsp;&nbsp;<?php  
                                                            $idadmin=$_COOKIE['username'];
                                                            echo $idadmin;
                                                                ?></a>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        <li class="active">
                            <a href="../admin/profileAdmin.php">Profile</a>
                        </li>
                        <li>
                            <a href="../admin/createAccount.php">Create Account</a>
                        </li>
                        <li>
                            <a href="../admin/createCourse.php">Create Course</a>
                        </li>
                        <li>
                            <a href="../admin/printPassStu.php">Print</a>
                        </li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <a href="../login.php">Logout</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <div class="col-sm-offset-1 col-md-10">
            <h1>Print Account Data</h1>
            <hr>
        </div>

        <div class="tabs">
            <ul class="nav nav-tabs">
                <li class="active"><a href="printPassStu.php">Student</a></li>
                <li><a href="printPassTeac.php">Teacher</a></li>
            </ul>
        </div>
        <div class="showtable">
            <table id="passStuTable" class="table table-bordered">
                <thead>
                    <tr>
                        <th>Selection</th>
                        <th>Username</th>
                        <th>Password</th>
                    </tr>
                </thead>
                <tbody>
                        <?php
                        require_once __DIR__."/databaseconnect.php";
                        $db=new Database();
                        $db->connect();
                        $result=mysqli_query($db->getConn(),"SELECT student.idstudent,student.password FROM student ");
                        while($row=mysqli_fetch_array($result)){
                            echo '<tr>';
                            echo '<td><label><input type="checkbox" value=""></label></td>';
                            echo "<td>$row[0]</td>";
                            echo "<td>$row[1]</td>";
                            echo '</tr>';
                        }
                        ?>
                </tbody>
            </table>
        </div>
        <div class="form-group">
            <button id="submit" type="button" name="submit" onclick="createFile()" class="btn btn-success">Print</button>
        </div>

        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script>
            window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')
        </script>
        <script src="js/vendor/bootstrap.min.js"></script>
        <script type="text/javascript" src="jspdf.min.js"></script>
        <script type="text/javascript" src="jspdf.plugin.autotable.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf-autotable/2.3.0/jspdf.plugin.autotable.js"></script>
        <script>
            var points = [];
            var username = [];
            //var password=[];
            (function (b, o, i, l, e, r) {
                b.GoogleAnalyticsObject = l;
                b[l] || (b[l] =
                        function () {
                            (b[l].q = b[l].q || []).push(arguments)
                        });
                b[l].l = +new Date;
                e = o.createElement(i);
                r = o.getElementsByTagName(i)[0];
                e.src = '//www.google-analytics.com/analytics.js';
                r.parentNode.insertBefore(e, r)
            }(window, document, 'script', 'ga'));
            ga('create', 'UA-XXXXX-X', 'auto');
            ga('send', 'pageview');
            
            $('#passStuTable').find('tr').click( function(){
                //alert('You clicked row '+ ($(this).index()+1) );
                var index = points.indexOf($(this).index()+1);
                var chooseUser=(document.getElementById("passStuTable").rows[$(this).index()+1].cells[1].innerHTML);
                var choosePass=(document.getElementById("passStuTable").rows[$(this).index()+1].cells[2].innerHTML);
                if (index > -1) {
                    username.splice(chooseUser,1);
                    points.splice(index, 1);
                }
                else{
                    points.push($(this).index()+1);                    
                    username.push(new Array(document.getElementById("passStuTable").rows[$(this).index()+1].cells[1].innerHTML,document.getElementById("passStuTable").rows[$(this).index()+1].cells[2].innerHTML));
                    console.log(points);
                    console.log(username);
                    
                    //alert(document.getElementById("passStuTable").rows[$(this).index()+1].cells[1].innerHTML);
                    //alert(document.getElementById("passStuTable").rows[$(this).index()+1].cells[2].innerHTML);
                }
            });
            function createFile(){
                //alert("AAAA");
                var columns = ["ID", "Password"];
                var rows = [
                            ["Shaw", "Tanzania"],
                            ["Nelson", "Kazakhstan"],
                            ["Garcia", "Madagascar"],
                        ];
                //alert (username);
                //alert (rows);
                var doc = new jsPDF();
                doc.autoTable(columns, username);
                doc.save('table.pdf');
            }
        </script>
        

    </body>
</html>