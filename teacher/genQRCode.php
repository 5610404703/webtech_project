<html>
    <head>
        <meta charset="UTF-8">
        <title>Genarate QR Code</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <link rel="apple-touch-icon" href="../apple-touch-icon.png">
        <link rel="stylesheet" href="../css/bootstrap.min.css">
        <link rel="stylesheet" href="../css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="../css/createAccStyle.css">
        <script src="../js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>

        <style>
            .containerQR {
                text-align: center;
            }
        </style>
    </head>
    <body>
        <nav class="navbar navbar-fixed-top" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="../teacher/profileTeacher.html"><span><img alt="Brand" src="../picture/display.png" width="30" height="30"></span>&nbsp;&nbsp;Username</a>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="../teacher/profileTeacher.html">Profile</a></li>
                        <li><a href="../teacher/searchSubject.html">Search Course</a></li>

                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <!--<li><img src ="picture/display.png" height="42" width="42"><span id="idDisplay">ID: </span></li>-->
                        <li><a href="../login.html">Logout</a></li>
                    </ul>
                </div>
            </div>
        </nav>
        <?php
        //display generated file
        $errorCorrectionLevel = "";
        $matrixPointSize = "";
        $filename = "";
        $PNG_WEB_DIR = "";
        $text = "";
        ?>
        <div class="containerQR">
            <img src=<?php echo "' . $PNG_WEB_DIR . basename($filename) . '"; ?> /><hr/>

            <!--config form-->

            <form class="form-inline" action="index.php" method="post">
                <div class="form-group">
                    <label for="data">Data : </label>
                    <input type="text" class="form-control" value="<?php echo $text; ?>" /> &nbsp; &nbsp;
                </div>
                <div class="form-group">
                    <label for="ecc">ECC : </label>
                    <select class="form-control" name="level">
                        <option value="L"<?php echo (($errorCorrectionLevel == 'L') ? ' selected' : ''); ?>> L - smallest</option>
                        <option value="M"<?php echo (($errorCorrectionLevel == 'M') ? ' selected' : ''); ?>>M</option>
                        <option value="Q"<?php echo (($errorCorrectionLevel == 'Q') ? ' selected' : ''); ?>>Q</option>
                        <option value="H"<?php echo (($errorCorrectionLevel == 'H') ? ' selected' : ''); ?>>H - best</option>
                    </select> &nbsp; &nbsp;
                </div>
                <div class="form-group">
                    <label for="size">Size : </label>
                    <select class="form-control" name="size">

                        <?php for ($i = 1; $i <= 10; $i++)

                            ?>

                        <option value="<?php echo $i; ?>" <?php echo (($matrixPointSize == $i) ? ' selected' : ''); ?>> <?php echo $i; ?> </option>;

                    </select> &nbsp; &nbsp; &nbsp; &nbsp;
                </div>


                <input type="submit" class="btn btn-primary" value="GENERATE"></form><hr/>
        </form>
    </div>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>

    <script src="../js/vendor/bootstrap.min.js"></script>

    <script>
        (function (b, o, i, l, e, r) {
            b.GoogleAnalyticsObject = l;
            b[l] || (b[l] =
                    function () {
                        (b[l].q = b[l].q || []).push(arguments)
                    });
            b[l].l = +new Date;
            e = o.createElement(i);
            r = o.getElementsByTagName(i)[0];
            e.src = '//www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e, r)
        }(window, document, 'script', 'ga'));
        ga('create', 'UA-XXXXX-X', 'auto');
        ga('send', 'pageview');
    </script>
</body>
</html>
