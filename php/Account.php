<?php

require_once __DIR__."/Account.php";
class Account {
  protected $username;
  protected $password;
  protected $profilePic;

  //สร้างบัญชีครั้งแรกโดยไม่ใส่รูปโปรไฟล์
  public function __construct($username, $password) {
    $this->username = $username;
    $this->password = $password;
    $this->profilePic=new Picture();
  }

  public function getUsername() {
    return $this->username;
  }

  public function changeUsername() {
    $this->username = $username;
  }

  public function getPassword() {
    return $this->password;
  }

  public function changePassword($password) {
    $this->password = $password;
  }

  

}

?>
