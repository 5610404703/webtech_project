<?php

class Teacher extends Account {
  private $permission;

  function __construct($username, $password) {
    //$this->username เป็นตัวแปรจากคลาส Account ซึ่งเป็นคลาสแม่
    $this->username = $username;
    $this->password = $password;
  }

  private function getPermission() {
    return $this->permission;
  }

  private function setPermission($permission) {
    $this->permission = $permission;
  }

}

?>
