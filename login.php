<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>Login</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">

        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <link rel="stylesheet" href="css/bootstrap.min.css">

        <link rel="stylesheet" href="css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="css/loginstyle.css">

        <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    </head>
    <body>

        <div class="container">

            <form class="form-signin" method="POST" >
                <h2 class="form-signin-heading">Login</h2>
                <label for="inputID" class="sr-only">ID</label>
                <input type="text" name="username" id="inputID" class="form-control" placeholder="ID" required autofocus>
                <label for="inputPassword" class="sr-only">Password</label>
                <input type="password" name="password" id="inputPassword" class="form-control" placeholder="Password" required>
                <div class="checkbox">
                    <label>
                        <input type="checkbox" value="remember-me"> Remember me
                    </label>
                </div>
                <button class="btn btn-lg btn-primary btn-block" name="submit" type="submit">Login</button>
            </form>

        </div>

        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>

        <script src="js/vendor/bootstrap.min.js"></script>

        <script>
            (function (b, o, i, l, e, r) {
                b.GoogleAnalyticsObject = l;
                b[l] || (b[l] =
                        function () {
                            (b[l].q = b[l].q || []).push(arguments)
                        });
                b[l].l = +new Date;
                e = o.createElement(i);
                r = o.getElementsByTagName(i)[0];
                e.src = '//www.google-analytics.com/analytics.js';
                r.parentNode.insertBefore(e, r)
            }(window, document, 'script', 'ga'));
            ga('create', 'UA-XXXXX-X', 'auto');
            ga('send', 'pageview');
        </script>
    </body>
    <?php
        if (isset($_POST['submit'])){
        $servername = "127.0.0.1";
        $username = "root";
        $password = "";
        $dbname = "personal";
        // Create connection
        //$sql = "INSERT INTO MyGuests (firstname, lastname, email)
        //VALUES ('John', 'Doe', 'john@example.com')";
        $conn = new mysqli($servername, $username, $password, $dbname);
        $sql = "SELECT * FROM admin";
        $result = $conn->query($sql);
        if ($result->num_rows > 0) {
            // output data of each row
            while($row = $result->fetch_assoc()) {
                if ($row['idadmin']==$_POST['username'] && $row['password']==$_POST['password']){
                    //echo $row['firstname']."    ".$row['lastname'];
                    setcookie('firstname',$row['firstname']);
                    setcookie('lastname',$row['lastname']);
                    setcookie('username',$row['idadmin']);
                    setcookie('password',$row['password']);
                    setcookie('idpicture',$row['idpicture']);
                    setcookie('type',$row['type']);
                    header('Location: admin/profileAdmin.php');
                }
                //else{
                //  echo "Can't Login";
                //  break;
                //}
            }
        }
        $sql = "SELECT * FROM teacher";
        $result = $conn->query($sql);
        if ($result->num_rows > 0) {
            // output data of each row
            while($row = $result->fetch_assoc()) {
                if ($row['idteacher']==$_POST['username'] && $row['password']==$_POST['password']){
                    //echo $row['firstname']."    ".$row['lastname'];
                    setcookie('firstname',$row['firstname']);
                    setcookie('lastname',$row['lastname']);
                    setcookie('username',$row['idteacher']);
                    setcookie('password',$row['password']);
                    setcookie('idpicture',$row['idpicture']);
                    setcookie('type',$row['type']);
                    header('Location: teacher/profileTeacher.php');
                }
                //else{
                //  echo "Can't Login";
                //  break;
                //}
            }
        }
        $sql = "SELECT * FROM student";
        $result = $conn->query($sql);
        if ($result->num_rows > 0) {
            // output data of each row
            while($row = $result->fetch_assoc()) {
                if ($row['idstudent']==$_POST['username'] && $row['password']==$_POST['password']){
                    //echo $row['firstname']."    ".$row['lastname'];
                    setcookie('firstname',$row['firstname']);
                    setcookie('lastname',$row['lastname']);
                    setcookie('username',$row['idstudent']);
                    setcookie('password',$row['password']);
                    setcookie('idpicture',$row['idpicture']);
                    setcookie('type',$row['type']);
                    header('Location: student/profileStudent.php');
                }
                //else{
                //  echo "Can't Login";
                //  break;
                //}
            }
        }

        else
        {
            return false;
        }
        $conn->close();
    }
    ?>
</html>
